python-smstrade (0.2.4-7) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.

 -- Ondřej Nový <onovy@debian.org>  Fri, 18 Oct 2019 15:54:44 +0200

python-smstrade (0.2.4-6) unstable; urgency=medium

  * Team upload.

  [ Scott Kitterman ]
  * Remove python-smstrade Recommends: python3-smstrade to keep python and
    python3 runtime segrated (per Python policy)
    - The scripts in python3-smstrade do not use python-smstrade even if
      installed

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump standards version to 4.4.0 (no changes)
  * Bump debhelper compat level to 12

 -- Ondřej Nový <onovy@debian.org>  Fri, 26 Jul 2019 19:48:36 +0200

python-smstrade (0.2.4-5) unstable; urgency=medium

  * Fix "removal of python3-smstrade/stretch makes files disappear from
    python-smstrade/jessie" by adding Breaks to python3-smstrade, add
    Recommends python3-smstrade to python-smstrade (Closes: #810594)

 -- Jan Dittberner <jandd@debian.org>  Sun, 10 Jan 2016 15:38:52 +0100

python-smstrade (0.2.4-4) unstable; urgency=medium

  * change debian/watch to PyPI redirector
  * Use fixed upstream version instead of using vcversioner by patching
    version files and setup.py, remove vcversioner from Build-Depends
  * Move manpages to python3-smstrade
  * Use python3 scripts instead of python2 scripts
  * Add lintian overrides for the generated scripts
  * Use dh_override_auto_test to pass --system=custom and run tests in
    {build_dir}
  * Update copyright years
  * Build-Depend on python-all and python3-all to run tests with all
    supported Python versions
  * Copy tests to build_dir before tests, remove after tests
  * Bump Standards-Version to 3.9.6 (no changes)
  * add debian/python-smstrade.NEWS to announce that the scripts have
    been moved to the python3-smstrade package

 -- Jan Dittberner <jandd@debian.org>  Sat, 02 Jan 2016 21:23:18 +0100

python-smstrade (0.2.4-3) unstable; urgency=medium

  * add Python 3 support
    - new binary package python3-smstrade
    - move docs and examples to separate binary package python-smstrade-doc
    - add python3 Build-Depends and Depends lines
    - change debian/rules to use PYBUILD_* variables and change --buildsystem
      to pybuild, add python3 to --with
  * renames:
    - debian/manpages -> debian/python-smstrade.manpages
    - debian/docs -> debian/python-smstrade-doc.docs
    - debian/examples -> debian/python-smstrade-doc.examples

 -- Jan Dittberner <jandd@debian.org>  Thu, 05 Jun 2014 00:02:50 +0200

python-smstrade (0.2.4-2) unstable; urgency=medium

  * add python-six to Depends and Build-Depends

 -- Jan Dittberner <jandd@debian.org>  Wed, 04 Jun 2014 22:52:01 +0200

python-smstrade (0.2.4-1) unstable; urgency=medium

  * New upstream version.
  * upstream build uses current vcversioner too (Closes: #750346)

 -- Jan Dittberner <jandd@debian.org>  Wed, 04 Jun 2014 22:39:23 +0200

python-smstrade (0.2.3-1) unstable; urgency=medium

  * Initial release. (Closes: #739700)

 -- Jan Dittberner <jandd@debian.org>  Fri, 21 Feb 2014 17:46:07 +0100
